from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    main_todo = TodoList.objects.get(id=id)
    todo_list = main_todo.items.all()
    context = {"todo": main_todo, "todo_list": todo_list}
    return render(request, "todos/todo_detail.html", context)


def todo_list_create(request):
    form = TodoListForm
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            save_form = form.save(commit=False)
            save_form.save()
            return redirect("todo_list_detail", id=save_form.id)
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def todo_list_update(request, id):
    todo = TodoList.objects.get(id=id)
    form = TodoListForm(initial={"id": todo.id, "name": todo.name})
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo)
        if form.is_valid():
            save_form = form.save(commit=False)
            save_form.save()
            return redirect("todo_list_detail", id=save_form.id)
    context = {"form": form, "todo": todo}
    return render(request, "todos/todo_list_update.html", context)


def todo_list_delete(request, id):
    todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    return render(
        request,
        "todos/todo_list_delete.html",
        context={"todo": todo},
    )


def todo_item_create(request):
    # todo_list = TodoList.objects.get(id=id)
    todo_item_form = TodoItemForm
    if request.method == "POST":
        todo_item_form = TodoItemForm(request.POST)
        if todo_item_form.is_valid():
            form = todo_item_form.save(commit=False)
            form.save()
            print(form.list.id)
            return redirect("todo_list_detail", id=form.list.id)
    context = {
        # "todo_list": todo_list,
        "todo_item_form": todo_item_form,
    }
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    # todo_item = TodoList.objects.get(id=id)
    todo_item = TodoItem.objects.get(id=id)
    form = TodoItemForm(
        initial={
            "task": todo_item.task,
            "due_date": todo_item.due_date,
            "is_completed": todo_item.is_completed,
            "list": todo_item.list,
        }
    )
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    context = {"form": form}
    return render(request, "todos/todo_item_update.html", context)
